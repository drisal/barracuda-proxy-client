package com.barracuda.grpc.proto;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.Arrays;

public class ProxyClient {
    public static void main(String[] args) {
        // Channel is the abstraction to connect to a service endpoint
        // Let's use plaintext communication because we don't have certs
        final ManagedChannel channel = ManagedChannelBuilder.forTarget("localhost:10000").usePlaintext(true).build();
        BlobProxyGrpc.BlobProxyBlockingStub stub;
        stub = BlobProxyGrpc.newBlockingStub(channel);
        ReadBlobRequest request = ReadBlobRequest.newBuilder().setKey("lorum.txt").build();

        // Finally, make the call using the stub
        ReadBlobResponse response =  stub.readBlob(request);
        System.out.println("The response file in array of byte are :" + Arrays.toString(response.getData().toByteArray()));
    }
}
